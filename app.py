from flask import Flask, render_template, request
import hashlib
from connections.connections import Connections

mydb=Connections.connect_db()
mycursor = mydb.cursor()

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/login')
def login():
    return render_template('dashboard.html')

@app.route('/register',methods=['GET','POST'])
def register_user():
    if request.method == "POST":
        sql = "INSERT INTO users (first_name, last_name,email,password) VALUES (%s, %s,%s, %s)"
        val = (request.form['first_name'], request.form['last_name'], request.form['email'], request.form['pass'])
        mycursor.execute(sql, val)
        mydb.commit()

    return render_template('register.html')

@app.route('/home')
def home():
    return render_template('dashboard.html')

if __name__ == '__main__':
    app.run(debug = True)


