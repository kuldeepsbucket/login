import mysql.connector

class Connections:
  @staticmethod
  def connect_db():
    mydb = mysql.connector.connect(
      host="localhost",
      user="root",
      passwd="",
      database="test"
    )

    return mydb